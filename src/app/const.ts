export const DEFAULT_TIME_TO_RETAIN_CACHE = 10*60*1000;
export const QUERY_API = 'https://www.alphavantage.co/query';