import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Subject ,of } from "rxjs";
import { debounceTime, switchMap } from 'rxjs/operators';
import { DataService } from "../../services/data.service";
@Component({
  selector: "auto-complete",
  templateUrl: "./auto.complete.component.html",
  styleUrls: ['./auto.complete.component.scss']
})
export class AutoCompleteComponent {

  private dataInputSubject = new Subject<string>(); 
  public data = [
  ];
  public showLoader = false;
  public searchText: string ='';
  public showErr: boolean =false;

  keyword = "1. symbol";
  constructor(private dataService: DataService, private router: Router) {}
  
  ngOnInit() {
    this.dataInputSubject.asObservable().pipe(
      debounceTime(400),
      switchMap((text:string)=> {
         this.showLoader = true;
         return this.dataService.getSuggestion(text)
       })
   ).subscribe((data:any)=> {
     this.showLoader = false;
     this.data = data.bestMatches;
    });
  }
  onChangeSearch(search:string) {
    this.showErr = false;
    this.showLoader = true;
    this.searchText = search;
    this.data = [];
    if(this.searchText) {
      // Triggering the data stream with value
      this.dataInputSubject.next(search);
    } else {
      this.showLoader = false;
    }
  }
  selectEvent(e:any){
      this.router.navigate(['/stocks',e['1. symbol']]);
  }
  inputCleared(){
    this.showErr = false;
    this.data =[]
    this.searchText = '';
  }
  submitAutoComplete(e:any){
    e.preventDefault();
    let searchedItem = this.data.find((item)=>{
      return this.searchText.toUpperCase() === item['1. symbol'];
    })
    if(searchedItem) {
      this.selectEvent(searchedItem);
    } else {
      this.showErr = true;
    }
  }
}
