import { Component } from "@angular/core";
import { DataService } from "../../services/data.service";
@Component({
  selector: "home",
  templateUrl: "./home.component.html"
})
export class HomeComponent {
  data = [
  ];
  keyword = "1. symbol";
  constructor(private dataService: DataService) {}
  onChangeSearch(search:string) {
    this.data = [];
    if(search.length>2) {
      this.dataService.getSuggestion(search).subscribe((data: any) => {
        this.data = data.bestMatches;
      });
    }
  }
  selectEvent(e:any){
      console.log(e);
  }
}
