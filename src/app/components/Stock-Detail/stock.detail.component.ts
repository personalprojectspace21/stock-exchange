import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Router } from "@angular/router";
import { throwError } from 'rxjs';
import { DataService } from "../../services/data.service";
@Component({
  selector: "stock-detail",
  templateUrl: "./stock.detail.component.html",
  styleUrls: ['./stock.detail.component.scss']
})
export class StockDetailComponent {
  subscription: any;
  stockDetails: any;
  indexInSearchStack: number=-1;
  searchStack: string[] = [];
  timeToRetainCache: number = this.dataService.getTimeToRetainCache()/(60*1000);
  constructor(private route: ActivatedRoute,
     private router: Router,
     private dataService: DataService) {}
  ngOnInit(): void {
    this. subscription = this.route.paramMap.subscribe((params: ParamMap) => {
      let stockSymbol = <string>params.get('symbol');
      // On route ChangeDetectorRef, fetching the symbol specific this.stockDetails
      // either from cahce or server
      this.getStockDetail(stockSymbol);
      if(this.indexInSearchStack === -1) {
        this.updateSearchStack(stockSymbol);
      }
    });
    
  }

  // Based on previous search item, pushes the current item in the 
  // search stack for navigating through previously searched values
  updateSearchStack(symbol:string) {
    this.searchStack = JSON.parse(<string>localStorage.getItem('searchStack'));
    if(this.searchStack) {
       let index = this.searchStack.findIndex((item:string)=> item === symbol);
       if(index !== -1) {
        this.searchStack.splice(index,1);
       }
    } else {
      this.searchStack = [];
    }
    this.searchStack.push(symbol);
    localStorage.setItem('searchStack',JSON.stringify(this.searchStack));
    this.indexInSearchStack = this.searchStack.length-1;
  }

  getPrevDetails(){
    this.indexInSearchStack-=1;
    this.router.navigate(['/stocks',this.searchStack[this.indexInSearchStack]]);
  }
  getNextDetails(){
    this.indexInSearchStack+=1;
    this.router.navigate(['/stocks',this.searchStack[this.indexInSearchStack]]);
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getStockDetail(symbol: string) {
      this.dataService.getStockDetails(symbol)
      .subscribe(data=>{
          this.stockDetails = data;
      });
  } 
  updateDataRefereshTime(e:any){
    this.dataService.setTimeToRetainCache(e.target.value*60*1000);
  }
}
