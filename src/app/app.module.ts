import { BrowserModule } from "@angular/platform-browser";
import { NgModule , Injectable} from "@angular/core";
import { AutocompleteLibModule } from "angular-ng-autocomplete";
import { AppComponent } from "./app.component";
import { AutoCompleteComponent } from "./components/autocomplete/auto.complete.component";
import { HttpClientModule ,HTTP_INTERCEPTORS} from "@angular/common/http";
import { StockDetailComponent } from "./components/Stock-Detail/stock.detail.component";
import { HomeComponent } from "./components/Home/home.component";
import { AppRoutingModule } from "./app.routing.module";
import { FormsModule }   from '@angular/forms';
 import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [AutoCompleteComponent, AppComponent,StockDetailComponent,HomeComponent],
  imports: [BrowserModule, AutocompleteLibModule, HttpClientModule,AppRoutingModule,FormsModule,ReactiveFormsModule],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

