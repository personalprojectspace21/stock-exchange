import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { StockDetailComponent } from "./components/Stock-Detail/stock.detail.component";
import { HomeComponent } from "./components/Home/home.component";
@NgModule({
    imports: [RouterModule.forRoot([
       { path:'',
        redirectTo: 'stocks',
        pathMatch: 'full'
    },
        { path:'stocks',
        component: HomeComponent
    },
    { path:'stocks/:symbol',
        component: StockDetailComponent
    },
        { path:'**',
        redirectTo: 'stocks'}
    ])],
    exports:[RouterModule]
  })
  export class AppRoutingModule {}