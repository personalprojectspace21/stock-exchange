import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { of } from "rxjs";
import { map } from "rxjs/operators";
import { DEFAULT_TIME_TO_RETAIN_CACHE, QUERY_API } from './../const';
import { keyframes } from "@angular/animations";
@Injectable({
  providedIn: "root"
})
export class DataService {
  constructor(private http: HttpClient) {
    if(!this.getTimeToRetainCache()) {
      this.setTimeToRetainCache(DEFAULT_TIME_TO_RETAIN_CACHE);
    } 
  }
  setTimeToRetainCache(time: number) {
    localStorage.setItem('dataRefereshTime',JSON.stringify(time));
  }
  getTimeToRetainCache() { 
    return parseInt(JSON.parse(<string>localStorage.getItem('dataRefereshTime')));
  }
  private getDataFromChache(storeKey: string,query: string,url: string) {
     
     let storeData = JSON.parse(<string>localStorage.getItem(storeKey));
     let objData = storeData?storeData[query]: null;
     let currTime = new Date().getTime();

    //  Checking if cache time is over
     if(objData && objData.value && (currTime-objData.lastFetched)<this.getTimeToRetainCache()) {
       return of(objData.value);
     } else {
      return this.http.get(url).pipe(map((data)=>{
        
        //  Queried data is stored in a map like structure where query is a key
        //  and response is value
        storeData= storeData?storeData : {};
        storeData[query] = {
          lastFetched:currTime,
          value: data
        }
        localStorage.setItem(storeKey,JSON.stringify(storeData));
        return data;
      }));
     }
  }
  getSuggestion(topic:string) {
     let url = `${QUERY_API}?function=SYMBOL_SEARCH&keywords=${topic}&apikey=GATTCOWZ0VG8XAA0`;
     return this.getDataFromChache('suggestions',topic,url);
  }
  
  getStockDetails(symbol:string) {
     let url = `${QUERY_API}?function=OVERVIEW&symbol=${symbol}&apikey=GATTCOWZ0VG8XAA0`;
     return this.getDataFromChache('stockDetails',symbol,url);
  }
}
